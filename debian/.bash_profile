# ~/.bash_profile: executed by bash(1) for login shells.
# see /usr/share/doc/bash/examples/startup-files for examples

#umask 002

export EDITOR='emacs'

# A partir de abajo puede estar obsoleto

# para usar el cvs

#export CVSROOT=:ext:user@maquina:ruta-del-cvs
#export CVS_RSH='ssh'

# para usar el pvm

# export PVM_ROOT=/usr/lib/pvm3
# export PVM_ARCH=/home/user/programacion/pvm/

# # para usar el java

# export JAVA_HOME=/usr/lib/j2se/1.4/jre/
# export CLASS_PATH=/usr/share/java/

# # execute .bashrc if it exists.

# if [ -e ~/.bashrc ]
# then
#   . ~/.bashrc
# fi

# LC_MESSAGES=gl_ES

